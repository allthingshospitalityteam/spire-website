$(document).ready(function () {
  const toggleText = [
    "Show Restaurants",
    "Show Properties"
  ]
  let toggleState = 0;

  const cards = $('#cards')
  const all = $('.card').toArray()
  const restaurants = all.filter(el => $(el).attr('data-brand').toLowerCase() === 'res')
  const properties = all.filter(el => $(el).attr('data-brand').toLowerCase() !== 'res')

  var $btns = $('.btn').click(function () {
    switch (this.id) {
      case 'all':
        $('#cards > div').fadeIn(450);
        break;
      case 'brand-filter':
        $(".brands").toggle();
        $(".locations").hide();
        revertAndAppendAll();
        break;
      case 'location-filter':
        $(".locations").toggle();
        $(".brands").hide();
        revertAndAppendAll();
        break;
      case 'type-toggle':
        toggle(this);
        break;
      default:
        revertAllClasses()
        const filtered = all.filter(notIncluding, this.id)

        cards.children().toArray().forEach(el => {
          if (!filtered.includes(el)) {
            $(el).detach()
          }
        })

        all.forEach(el => {
          if (filtered.includes(el)) {
            appendWithFadeIn(el)
          }
        })
    }

    $btns.removeClass('active');
    $(this).addClass('active');
  })

  // Must not detach elements before the click events have been added.
  restaurants.forEach(el => $(el).detach());

  function notIncluding (el) {
    if ($(el).attr('data-brand') === this.toString() || $(el).attr('data-state') === this.toString())
            return el
  }

  function revertAllClasses() {
    all.forEach(el => {
      $(el)
        .removeClass('is-expanded is-inactive')
        .addClass('is-collapsed')
    })
  }

  function appendAll() {
    all.forEach(el =>
      appendWithFadeIn(el))
  }

  function appendWithFadeIn(el) {
    $(el).appendTo(cards).fadeIn(450)
  }

  function revertAndAppendAll() {
    revertAllClasses()
    appendAll()
  }
  
  function toggle(btn) {
    toggleState % 2 === 0 
      ? show(restaurants).hide(properties)
      : show(properties).hide(restaurants)

    $(btn).html(toggleText[toggleState % 2])

    function show(toShow) {
      return {
        hide: function(toHide) {
          toShow.forEach(el => $(el).appendTo(cards))
          toHide.forEach(el => $(el).detach())
          toggleState++
        }
      }
    }
  }
})

	