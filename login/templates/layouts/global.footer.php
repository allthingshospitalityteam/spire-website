<style>
  .flex {
    display: flex;
    align-items: center;
  }

  .map-wrapper {
    width: 100%;
    height: 100%;
  }

  .map {
    width: 100%;
    min-height: 20rem;
  }

  .foottext p {
    font-size: 1.4rem;
    margin-bottom: 0px;
  }

  .left-column-text {
    font-size: 2rem !important;
    margin-bottom: 2rem !important;
    font-weight: 700;
    text-transform: uppercase;
  }

  .footer-logo {
    width: 60%;
    display: block;
    margin: 0 auto;
  }

  @media screen and (max-width: 812px) {
    .flex {
      flex-direction: column-reverse;
    }

    .footer-logo {
      margin: 1rem auto;
    }
  }
</style>

<div class="section darkblue p50 foottext">
  <div class="container">
    <div class="row flex">
      <div class="four columns center left-column">
        <?php perch_content('Footer Logo');?>
      </div>
      <div class="four columns center">
        <?php perch_content('Company Information');?>
      </div>
      <div class="four columns">
        <div class="map-wrapper">
          <div class="map" id="irving-map"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <script src="https://athtestsites.com/sal/sal.js"></script> -->
<script src="/scripts/sal.js"></script>

<script>
  sal();
</script>

<script>
  $("nav div").click(function () {
    $("ul").slideToggle();
    $("ul ul").css("display", "none");
  });

  $("ul li").click(function () {
    $("ul ul").slideUp();
    $(this).find('ul').slideToggle();
  });

  $(window).resize(function () {
    if ($(window).width() > 768) {
      $("ul").removeAttr('style');
    }
  });
</script>
<script>
  $('ul li.has-children').on("touchstart", function (e) {
    'use strict'; //satisfy code inspectors
    var link = $(this); //preselect the link
    if (link.hasClass('hover')) {
      return true;
    } else {
      link.addClass('hover');
      $('ul > li').not(this).removeClass('hover');
      e.preventDefault();
      return false; //extra, and to make sure the function has consistent return points
    }
  });
</script>
<script>
  // When the user scrolls down 80px from the top of the document, resize the navbar's padding and the logo's font size
  window.onscroll = function () {
    onscroll_function()
  };

  function onscroll_function() {
    scrollFunction();
    // scrollFunction2();
  }

  function scrollFunction() {
    if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      document.getElementById("navbar").style.padding = "0px 0px";
      document.getElementById("navbar").style.backgroundColor = "#f2f2f2";
      // document.getElementById("navbar-left").style.padding = "1rem 0 1rem 1rem"
      document.getElementById("logo-img").style.width = "200px";
      // document.getElementById("logo-img").style.height = "67px";
      // document.getElementById("logo-img").style.margin = "5px 0px";
    } else {
      document.getElementById("navbar").style.padding = "30px 0px";
      document.getElementById("navbar").style.backgroundColor = "#fff";
      document.getElementById("logo-img").style.width = "250px";
      // document.getElementById("logo-img").style.width = "300px";
      // document.getElementById("logo-img").style.height = "100px";
      // document.getElementById("logo-img").style.margin = "0px 0px";
    }
  }

  function scrollFunction2() {
    if (document.body.scrollTop > 780 || document.documentElement.scrollTop > 780) {
      document.getElementById("nav").style.position = "fixed";
      document.getElementById("nav").style.top = "170px";
    } else {
      document.getElementById("nav").style.position = "relative";
      document.getElementById("nav").style.top = "0";
    }
  }
</script>
</body>
</html>