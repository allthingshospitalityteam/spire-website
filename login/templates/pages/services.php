<?php perch_layout('global.header'); ?>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
	integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<div class="first section ltgrey">
	<div class="row">
		<?php perch_content('Page Header');?>
	</div>
</div>

<?php perch_content('Services Contact Modal'); ?>
<?php perch_content('Approach'); ?>
<?php perch_content('Services');?>
<?php perch_content('Goal'); ?>

<?php perch_layout('global.footer'); ?>