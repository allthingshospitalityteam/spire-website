<?php perch_layout('global.header'); ?>
<link rel="stylesheet" href="/css/home/main.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<div class="first section">
   <div class="row">
      <div class="twelve columns">
         <?php perch_content('Header Slider'); ?>
      </div>
   </div>
</div>

<div class="section ltgrey">
   <div class="row">
      <?php perch_content('Home About Section');?>
   </div>
</div>

<div class="services section white">
   <div class="container">
      <div class="row">
         <div class="twelve columns p100">
            <?php perch_content('Home Services Section');?>
            <?php perch_content('Home Services Hover Boxes') ;?>
         </div>
      </div>
   </div>
</div>

<div class="section ltgrey">
   <div class="row"><?php perch_content('Join our Team');?></div>
</div>

<?php perch_layout('global.footer'); ?>