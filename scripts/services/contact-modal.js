$(function() {
  const showClass = "contact-modal-showing"
  const $modal = $('.contact-modal-wrapper')
  const $overlay = $('.contact-modal-overlay')

  const urlParams = new URLSearchParams(window.location.search) 

  if (urlParams.has('submission'))
    openModal()
  
  $('.contact-person').click(openModal)

  function openModal() {
    $modal.addClass(showClass);
    $overlay.click(closeModal)
  }

  function closeModal() {
    $modal.removeClass(showClass);
  }
})