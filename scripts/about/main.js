// For Scrolling Buttons
const offset = 125;
$(function () {
  function scrollTo(e) {
    $('html, body').animate({
      scrollTop: $(e.target.dataset.anchor).offset().top - offset
    })
  }

  $('.scroll-control').click(scrollTo)
});