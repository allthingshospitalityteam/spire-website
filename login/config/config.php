<?php
//   define('PERCH_DEBUG', true);
    switch($_SERVER['SERVER_NAME']) {

        case 'spire.athgraphics.com':
            include(__DIR__.'/config.spire-athgraphics-com.php');
            break;

        case 'spire-website':
            include(__DIR__.'/config.local.php');
            break;

        default:
            include('config.production.php');
            break;
    }

    define('PERCH_LICENSE_KEY', 'R31910-WZC427-NDH062-RPT450-SZM002');
    define('PERCH_EMAIL_FROM', 'brittany@allthingshospitality.com');
    define('PERCH_EMAIL_FROM_NAME', 'Brittany Lewis');

    define('PERCH_LOGINPATH', '/login');
    define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', __DIR__));
    define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');

    define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'resources');
    define('PERCH_RESPATH', PERCH_LOGINPATH . '/resources');
    
    define('PERCH_HTML5', true);
    define('PERCH_TZ', 'UTC');
    
    define('PERCH_GMAPS_API_KEY', 'AIzaSyDz797LUB5SBrXeLqfnRhit0fcp2sWyxbw');
