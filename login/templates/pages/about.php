<!-- TODO: Make More Modular. Refactor To Make More Generic -->
<?php perch_layout('global.header'); ?>

<script src="/scripts/cards.min.js" async defer></script>
<link rel="stylesheet" href="/css/cards/main.min.css">
<link rel="stylesheet" href="/css/cards/about.min.css">
<link rel="stylesheet" href="/css/about/main.min.css">

<div class="first section white">
	<div class="row">
		<?php perch_content('About Page Intro'); ?>
	</div>
</div>

<div class="leadership-team-container">
	<?php perch_content('Executive Intro'); ?>
	<div class="cards">
		<?php perch_collection('Spire Team', [ 
				'filter'=> 'position',
				'match'=> 'eq',
				'value'=> "Executive Team",
			]);?>
	</div>
</div>

<div class="team-header-container" id="team-cards-header">
	
<?php perch_content('Team Selection Intro'); ?>
</div>

<div class="team-cards-container">
	<div class="team-card [ is-collapsed ] ">
		<div class="team-card-inner [ js-expander ]">
			<button class="button button-primary">Operations</button>
		</div>
		<div class="team-card-expander">
			<?php perch_content('Operations Intro'); ?>
			<div class="cards team-cards">
				<?php perch_collection('Spire Team', [ 
				'filter'=> 'position',
				'match'=> 'eq',
				'value'=> "Operations Team",
			]);?>
			</div>
		</div>
	</div>
	<div class="team-card [ is-collapsed ] ">
		<div class="team-card-inner [ js-expander ]">
			<button class="button button-primary">Sales, Marketing & Revenue</button>
		</div>
		<div class="team-card-expander">
			<?php perch_content('Sales Marketing Intro');?>
			<div class="cards team-cards">
				<?php perch_collection('Spire Team', [
				'filter'=> 'position',
				'match'=> 'eq',
				'value'=> "Sales Marketing Team",
			]);?>
			</div>
		</div>
	</div>
	<div class="team-card [ is-collapsed ] ">
		<div class="team-card-inner [ js-expander ]">
			<button class="button button-primary">Human Resources</button>
		</div>
		<div class="team-card-expander">
			<?php perch_content('Human Resources Intro');?>
			<div class="cards team-cards">
				<?php perch_collection('Spire Team', [
				'filter'=> 'position',
				'match'=> 'eq',
				'value'=> "Human Resources Team",
			]);?>
			</div>
		</div>
	</div>
	<div class="team-card [ is-collapsed ] ">
		<div class="team-card-inner [ js-expander ]">
			<button class="button button-primary">Accounting & Finance</button>
		</div>
		<div class="team-card-expander">
			<?php perch_content('Accounting & Finance Intro');?>
			<div class="cards team-cards">
				<?php perch_collection('Spire Team', [
				'filter'=> 'position',
				'match'=> 'eq',
				'value'=> "Accounting & Finance",
			]);?>
			</div>
		</div>
	</div>
	<!-- <div class="team-card">
		<div class="team-card-inner">
			<button class="button button-primary scroll-control" data-anchor="#board-container">AWH Partners</button>
		</div>		
	</div> -->
	
</div>

<!-- <div id="board-container" class="board-container">
	<?php // perch_content('Board of Directors Intro');?>
</div> -->

<script src="/scripts/about/main.min.js"></script>

<?php perch_layout('global.footer'); ?>