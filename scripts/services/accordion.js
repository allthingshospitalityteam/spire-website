const offset = 125
$(function () {
  $("#accordion").accordion({
    heightStyle: "content",
    active: false,
    collapsible: true,
    activate: function (e, ui) {
      $('html, body').animate({
        scrollTop: ui.newHeader.offset().top - offset
      });
    }
  });
});