$(function () {
  mapboxgl.accessToken =
    'pk.eyJ1IjoiYXZlcnllbGxlciIsImEiOiJjamx3bGtwcHUwMHdnM3dtY2h6bTNuZmd2In0.xdACIps6VbrcL6f4VGNrhA';

  /**
   * Config holds static information
   */
  const config = {
    standard: {
      style: 'mapbox://styles/averyeller/ck3uk7evy0hcm1cpd4hler3j8',
      zoom: 3.5,
      minZoom: 3,
      maxZoom: 14
    },
    portfolio: {
      coordinates: [-98.5556199, 39.8097343]
    }
  }

  /**
   * Get functions gather information from HTML 
   */
  const get = {
    /**
     * @return - an array of data objects from factory.data
     */
    coordinates: () => {
      const data = []
      // Get non-restuarant .coordinates, send them to factory.data(), 
      // then push the new object to the array
      $('.coordinates').each((i, el) => {
        if ($(el).parent().data().brand !== "Res")
          data.push(factory.data($(el)))
      })
      return data
    },
    /**
     * @param identifier - part of a class definition to help identify information
     * @return a function that gets location information
     */
    location: identifier => ({
      /**
       * @param el - the base .coordinates element that we will be working from
       * @return the text of some child element
       */
      attachedTo: el => el
        .siblings('.card__expander')
        .children('.intext')
        .children(`.${identifier}-wrapper`)
        .children(`.location-${identifier}`)
        .text()
    })
  }

  /**
   * Factory functions return an object
   */
  const factory = {
    /**
     * @param name - the name of the map
     * @return - a new instantiation of MapboxGL
     */
    map: name => ({
      portfolio: new mapboxgl.Map({
        container: `${name}-map`,         // The element that will contain the map
        center: config[name].coordinates, // The initial map center
        ...config.standard                // Necessary configuration settings for MapboxGL
      })
    }),
    /**
     * @return - a formated geojson object with a full set of MapboxGL features
     */
    geojson: () => ({
      type: 'FeatureCollection',
      features: get.coordinates().map(factory.feature)
    }),
    /**
     * @param el - an HTML element with a .coordinates div
     */
    data: el => ({
      // Bool representing if the element is a restaurant or not
      // isRestaurant: el.parent().data().brand === "Res",
      // The coordinates pulled from the element's data-long & data-lat attributes
      coordinates: [el.data().long, el.data().lat],
      // Location will be used in the popup
      location: {
        name: get.location('name').attachedTo(el),
        address: get.location('address').attachedTo(el)
      }
    }),
    /**
     * @param property - the property's feature information
     * @return a properly formatted MapboxGL Feature object
     */
    feature: property => ({
      type: 'Feature',
      // isRestaurant: property.isRestaurant,
      location: property.location,
      geometry: {
        type: 'Point',
        coordinates: property.coordinates
      }
    }),
    /**
     * @param el - The new element representing the property location
     * @param feature - The MapboxGL feature object with added params
     * @return a new MapboxGL Marker
     */
    marker: (el, feature) =>
      new mapboxgl.Marker(el, {
        // anchor: feature.isRestaurant ? 'bottom-left' : 'bottom-right'
        anchor: 'bottom'
      })
      .setLngLat(feature.geometry.coordinates)
      .setPopup(factory.popup(feature))
      .addTo(map.portfolio),
    /**
     * @param feature - The MapboxGL feature object with added params
     * @return a new MabpoxGL Popup
     */
    popup: feature =>
      new mapboxgl.Popup()
      .setHTML(generate.popupHTML(feature.location))
  }

  /**
   * Generate functions create HTML
   */
  const generate = {
    /**
     * @param feature - The MapboxGL Feature data for specific property
     * Create a new element with class name dependent on whether it is a restaurant
     * Send this new element to the factory.marker
     */
    marker: function (feature) {
      var el = document.createElement('div');
      // el.className = `${feature.isRestaurant ? 'restaurant' : 'property'}-marker marker`;
      el.className = 'property-marker marker';
      factory.marker(el, feature)
    },
    /**
     * @param location - The location information that will display in the popup
     * @return a string containing the HTML for display
     */
    popupHTML: location => `
      <h3 class="map-popup-headline">${location.name}</h3>
      <p class="map-popup-text">${location.address}</p>
    `
  }

  // Create Map
  const map = factory.map('portfolio')
  // Create geojson data and generate a marker for each geojson feature
  factory.geojson()
    .features.forEach(generate.marker)
})