$(document).ready(function () {
  const offset = 150
  // Select all links with hashes
  $('a[href*="#"]')
    .not('[href="#"]') // Remove links that don't actually link to anything
    .not('[href="#0"]') // Remove links that don't actually link to anything
    .click(handleSmoothScrollWithOffset)

  function handleSmoothScrollWithOffset(event) {
    if (anchorOnThisPage(this)) {
      const target = getTargetFrom(this.hash)

      if (target.length) { // If the target exists,
        event.preventDefault(); // then prevent default
        runAnimationWithOffsetTo(target) // and run animation
      }
    }

    function anchorOnThisPage(page) {
      return location.pathname.replace(/^\//, '') == page.pathname.replace(/^\//, '') && location.hostname == page
        .hostname
    }

    function getTargetFrom(id) {
      return $(id) ? $(id) : $('[name=' + id.slice(1) + ']');
    }

    function runAnimationWithOffsetTo(target) {
      $('html, body').animate({
        scrollTop: target.offset().top - offset
      }, 1000, function () {
        const $target = $(target);
        // Remove all Highlights
        $('.serv').toArray().forEach(el => $(el).removeClass('service-highlight'))
        // Add highlight to target service
        $target.addClass('service-highlight')
        // Callback after animation
        // Must change focus!
        // var $target = $(target);
        // $target.focus();
        // if ($target.is(":focus")) { // Checking if the target was focused
        //   return false;
        // } else {
        //   $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
        //   $target.focus(); // Set focus again
        // };
      });
    }
  }
})