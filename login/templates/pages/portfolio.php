<?php perch_layout('global.header'); ?>

<!-- For Brand Carousel -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js" ></script>
<script src="scripts/brand-partners/carousel.min.js" async defer></script>
<!-- Page Specific Scripts -->
<script src="/scripts/cards.min.js" async defer></script>
<script src="scripts/portfolio/filter.min.js" async defer></script>
<!-- Page Specific Styles -->
<link rel="stylesheet" href="/css/cards/main.min.css">
<link rel="stylesheet" href="/css/cards/portfolio.min.css">
<link rel="stylesheet" href="/css/portfolio/main.min.css">

<?php perch_content('Portfolio Header');?>

<div class="section white p100 buffer">
	<div class="row">
		<div class="twelve columns center ptb30">
			<button class="btn button button-primary primary-filter" id="brand-filter">Search by Brand</button>
			<!-- <button class="btn button button-primary primary-filter" id="location-filter">Search by Location</button> -->
			<button class="btn button button-primary primary-filter" id="type-toggle">Show Restaurants</button>
			<div class="brands hidden">
				<?php perch_collection('Portfolio', [
					'template' => 'brand_filter.html',
					'sort' => 'propertybrand',
					'sort-order' => 'ASC',
				]);?>
			</div>
			<div class="locations hidden">
				<?php perch_collection('Portfolio', [
					'template' => 'state_filter.html',
					'sort' => 'state',
					'sort-order' => 'ASC',
				]);?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="twelve columns">
			<div id="cards" class="cards">
				<?php perch_collection('Portfolio', [
					'template' => 'portfolio_cards.html',
					'sort' => 'propname',
					'sort-order' => 'RAND',
				]);?>
			</div>
		</div>
	</div>
</div>

<div class="section red ptb30">
  <div class="container">
    <div class="row">
      <div class="tweleve columns">
        <center>
          <h3>Notable Brand Partners</h3>
          <div class="h-under"></div>
        </center>
        <div class="customer-logos">
          <?php perch_collection('Portfolio', [
              'template' => 'logo_slider.html',
              'sort' => 'propertybrand',
              'sort-order' => 'ASC',
              'filter' => 'hotel',
              'match'  => 'eq',
              'value'  => 'hotel',
            ]); ?>
          <?php perch_collection('Portfolio', [
              'template' => 'indep_logo_slider.html',
              'filter' => 'propertybrand',
              'match'  => 'eq',
              'value'  => 'Independent',
            ]); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php perch_layout('global.footer'); ?>