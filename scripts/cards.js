$(document).ready(function () {
  const offset = 125; 
  const cells = [
    {
      cellType: $('.card'),
      class: '.card'
    },
    {
      cellType: $('.team-card'),
      scrollTo: $('#team-cards-header'),
      class: '.team-card'
    }
  ]  

  cells.forEach(item => {
    if (item.cellType) {
      item.cellType.find('.js-expander').click(function () {
        const $thisCell = $(this).closest(item.class);          // Find the closest .card to the click event.
        if ($thisCell.hasClass('is-collapsed')) {               // If the card is collapsed,
          open($thisCell, item.cellType)                        // then open it,
          scrollTo(item.scrollTo ? item.scrollTo : $thisCell)   // scroll to the offset,
          setHideOnClickOutsideEvent($thisCell, item.cellType)  // and set a click event for anything outside of the card
        }
      });
    }

    if (item.class === '.card') {
      item.cellType.find('.js-collapser').click(function () {
        const $thisCell = $(this).closest(item.class);            // Find the closest .card to the click event.
        returnToDefaultState($thisCell, item.cellType)            // Return it to the default state.
      });
    }    
  })

  // Open a specific element
  function open(el, cellType) {
    // Any cell that is not this element will be collapsed
    cellType.not(el).removeClass('is-expanded').addClass('is-collapsed')//.addClass('is-inactive');
    // This element will be expanded
    $(el).removeClass('is-collapsed').addClass('is-expanded');
  }
  
  // Add a click event for anything outside of an opened card
  function setHideOnClickOutsideEvent(el, cellType) {
    // Add outsideClickListener to the document.
    document.addEventListener('click', outsideClickListener)
  
    function outsideClickListener(e) {
      const $target = $(e.target)           // $target is the click event target
      if (                                  // If
        !$target.closest(el).length &&      // the target is not on the element and
        $(el).hasClass('is-expanded')       // the element is expanded,
      ) {
        returnToDefaultState(el, cellType)  // then return the element to its default state
        removeOutsideClickListener()        // and remove this listener
      }
    }
  
    function removeOutsideClickListener() {
      // Remove outsideClickListener from the document
      document.removeEventListener('click', outsideClickListener)
    }
  }
  
  // Return all cells to a default closed state
  function returnToDefaultState(el, cellType) {
    // Collapse this cell
    $(el).removeClass('is-expanded').addClass('is-collapsed');
    // Make sure no cell of this type is inactive
    cellType.not(el).removeClass('is-inactive');
  }
  
  // Scroll to a specific element
  function scrollTo(el) {
    $('html, body').animate({             // Animate
      scrollTop: el.offset().top - offset // scrolling with an offset
    });
  }
})

// This code is important if we want the inactive stuff //
// if ($cell.not($thisCell).hasClass('is-inactive')) {
//   //do nothing
// } else {
//   $cell.not($thisCell) //.addClass('is-inactive');
// }