$('.content').each((i, el) =>
  $(el).addClass(i % 2 === 0 ?
    'even' :
    'odd'
  ));

$('.spacer').first()
  .addClass('first-spacer');
$('.spacer').last()
  .addClass('last-spacer');