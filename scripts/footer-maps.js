$(window).on('load', function () {

  mapboxgl.accessToken =
    'pk.eyJ1IjoiYXZlcnllbGxlciIsImEiOiJjamx3bGtwcHUwMHdnM3dtY2h6bTNuZmd2In0.xdACIps6VbrcL6f4VGNrhA';

  const config = {
    irving: {
      coordinates: [-96.947430, 32.869816]
    },
    standard: {
      style: 'mapbox://styles/averyeller/ck2mdad89046i1cl5qfh0ys8c',
      zoom: 13
    }
  }

  const map = {
    grapevine: mapFactory('irving'),
    // deerfield: mapFactory('deerfield')
  }

  const geojson = {
    type: 'FeatureCollection',
    features: [
      featureFactory(config.irving.coordinates)
    ]
  };

  geojson.features.forEach(function (marker) {
    new mapboxgl.Marker()
      .setLngLat(marker.geometry.coordinates)
      .addTo(map.grapevine);
  });

  function mapFactory(name) {
    return new mapboxgl.Map({
      container: `${name}-map`,
      center: config[name].coordinates,
      ...config.standard
    })
  }

  function featureFactory(coordinates) {
    return {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates
      }
    }
  }
})