<?php perch_layout('global.header'); ?>

<style>
	label {font-size: 1.8rem; text-transform: uppercase;}
</style>


<div class="first section ltgrey">
	<div class="row">
		<?php perch_content('Page Header');?>
	</div>
</div>
<div class="section white p100">
	<div class="container">
	<div class="row">
		<div class="six columns ptb30">
			<?php
  perch_content_custom('Company Information', [
  	'page'=>'/index.php',
    'template'=>'contact_company_info.html',
  ]);
?>
		</div>
		<div class="six columns ptb30">
			<?php perch_content('Contact Form');?>
			
		</div>
	</div>
	</div>
</div>


<?php perch_layout('global.footer'); ?>