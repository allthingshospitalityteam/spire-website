<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php perch_pages_title(); ?></title>
	<?php perch_page_attributes(); ?>
	<link rel="icon" type="image/png" href="images/favicon.png">
	<link rel="stylesheet" href="/css/normalize.min.css">
	<link rel="stylesheet" href="/css/skeleton.min.css">
	<link rel="stylesheet" href="/css/style.min.css">
	<link rel="stylesheet" href="/css/sal.min.css">
	<script src="https://kit.fontawesome.com/a30691a6f1.js" crossorigin="anonymous"></script>
	<link rel="stylesheet " href="//fonts.googleapis.com/css?family=Raleway:400,300,700" type="text/css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" />
	</script>
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js" />
	</script> -->

<!-- Should Probably Move To header. If page speed becomes issue, recommend removing mapbox in favor of image -->
<link href='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.css' rel='stylesheet' />
<script src='https://api.mapbox.com/mapbox-gl-js/v1.4.1/mapbox-gl.js' async></script>
<script src="/scripts/footer-maps.min.js" async defer></script>
</head>

<body>
	<div id="navbar">
		<div class="nav-container">
			<div id="navbar-left">
				<a href="/" id="home-link">
					<img id="logo-img" src="/images/spire-full-color-logo.png" alt="">
				</a>
			</div>
			
			<div id="navbar-right">
				<nav>
					<div><i class="fa fa-bars"></i></div>
					<?php perch_pages_navigation(); ?>
				</nav>
			</div>
		</div>
	</div>