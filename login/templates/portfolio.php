<?php perch_layout('global.header'); ?>

<style>
	.hidden {
		display: none
	}
</style>
<script src="./scripts/portfolio.js" async defer></script>
<div class="section ltgrey" style="margin-top: 100px;">
	<div class="row">
		<?php perch_content('Page Header');?>
	</div>
</div>

<div class="section white p100">

	<div class="row">
		<div class="twelve columns center ptb30">

			<button class="btn button button-primary primary-filter" id="brand-filter">Search by Brand</button>
			<button class="btn button button-primary primary-filter" id="location-filter">Search by Location</button>

			<div class="brands hidden">
				<?php perch_collection('Portfolio', [
					'template' => 'brand_filter.html',
				]); ?>
			</div>
			<div class="locations hidden">
				<?php perch_collection('Portfolio', [
					'template' => 'state_filter.html',
				]); ?>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="twelve columns">
			<div id="cards" class="cards">
				<?php perch_collection('Portfolio');	?>
			</div>
		</div>
	</div>

</div>

<script>
	$(document).ready(function () {
		// console.log("Howdy, friend!")

		const cards = $('#cards')
		const all = $('.card').toArray()

		var $btns = $('.btn').click(function () {
			if (this.id == 'all') {
				$('#cards > div').fadeIn(450);
			} else if (this.id === 'brand-filter') {
				$(".brands").toggle();
				$(".locations").hide();
				revertAndAppendAll();
			} else if (this.id === 'location-filter') {
				$(".locations").toggle();
				$(".brands").hide();
				revertAndAppendAll();
			} else {
				if ($(this).hasClass('button-secondary')) {

					revertAllClasses()

					const needle = this.id
					const filtered = all.filter(el => {
						if ($(el).attr('data-brand') === needle)
							return el
						else if ($(el).attr('data-state') === needle)
							return el
					})

					cards.children().toArray().forEach(el => {
						console.log('In Detach')
						if (!filtered.includes(el)) {
							$(el).detach()
						}
					})

					all.forEach(el => {
						console.log('In Attach')
						if (filtered.includes(el)) {
							$(cards).append(el).fadeIn(450)
						}
					})

				}

				var $el = $('.' + this.id).fadeIn(450);
				$('#cards > div').not($el).hide();
			}
			$btns.removeClass('active');
			$(this).addClass('active');
		})

		function revertAllClasses() {
			all.forEach(el => {
				$(el)
					.removeClass('is-expanded is-inactive')
					.addClass('is-collapsed')
			})
		}

		function appendAll() {
			all.forEach(el =>
				appendWithFadeIn(el))
		}

		function appendWithFadeIn(el) {
			// $(cards).append(el).fadeIn(450)
			$(el).appendTo(cards).fadeIn(450)
		}

		function revertAndAppendAll() {
			revertAllClasses()
			appendAll()
		}
	});
</script>



<?php perch_layout('global.footer'); ?>