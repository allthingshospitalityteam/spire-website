$('.approach-slider').slick({
  autoplay: true,
  nextArrow: `
     <button type="button" class="slick-next">
       <svg class="arrow" width="3rem" viewBox="0 0 128 128">
        <g>
          <polygon points="34.782,128 29.218,122.435 87.653,64.001 29.218,5.565 34.782,0 98.782,64.001 	"/>
        </g>				
      </svg>
    </button>
  `,
  prevArrow: `
     <button type="button" class="slick-prev">
       <svg class="arrow" width="3rem" viewBox="0 0 128 128">
        <g>
          <polygon points="34.782,128 29.218,122.435 87.653,64.001 29.218,5.565 34.782,0 98.782,64.001 	"/>
        </g>				
      </svg>
    </button>
  `
})